# LIS3781

## Alec Elsbernd

### Assignment 4 Requirments:

A high-volumehome office supply company contractsa database designer to develop a systemin orderto track its 
day-to-day business operations. The CFO needsan updated method for storing data,running reports, and 
makingbusiness decisions based upon trends and forecasts, as well as maintaininghistorical datadue to new 
governmental regulations.Here arethe mandatory business rules: 

- A sales representative has at least one customer, and each customer has at leastone sales repon any given day(as it is a high-volumeorganization).
- A customer places at least one order. However, each order is placed by only one customer.
- Each order contains at least one order line.Conversely, each order line is contained in exactly one order.
- Each product may be on a number of order lines.Though, each order line containsexactly one productid (though, each product id may have a quantity of more than one included, e.g., “oln_qty”).
- Each order is billed on one invoice, and each invoice is a bill for exactly one order(by only one customer).
- An invoice can have one (full),or can have many payments (partial). Though, each payment is made to only one invoice.
- A store hasmany invoices, but each invoice is associated with only one store.
- A vendor providesmany products, but each product is provided by only one vendor.
- Must track yearly history ofsales reps, including(also, see Entity-specific attributesbelow): yearly sales goal, yearly total sales, yearly total commission (in dollars and cents).
- Must track history of products, including: cost, price, and discount percentage (if any).



#### Assignment Screenshots:


![diagram](img/a4_erd.png)



![select satements](img/a41.png)



![select staements](img/a42.png)



![select statements](img/a43.png)


