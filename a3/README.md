# LIS3781

## Alec Elsbernd

### Assignment #3 Requirements:
- install Oracle
- Populate tables
- screenshot of sql code and query results

> #### Git commands w/short descriptions:

1. git init - creates a new repo or initialize an empty repo
2. git status -  shows the state of the working directory and the staging area
3. git add - adds all modified and new files in the current directory and prepares them to be pulled
4. git commit - records the changes you made to the local repo
5. git push - updates the remote repo with local changes
6. git pull - changes your local repo to that made on the remote repo
7. git branch - list branches that are connected

#### Assignment Screenshots:



![customer sql statements](img/a3.1.png)



![company sql](img/a3.2.png)



![select statement](img/insert.png)


![select statement](img/a3inserts.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
