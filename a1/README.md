# LIS3781 Advanced Database Analysis

## Alec Elsbernd

### Assignment #1 Requirements:
- Install AMMPS
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Bitbucket tutarial (bitbucketstationlocation)
- Provide git command description

#### Assignment Screenshots:

*Screenshot of data dictionary*:

![assignment 1 data dictionary](img/datadictionary1.png)

*Screenshot of a1 model*:

![assignment 1 model](img/a1model.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
