﻿# LIS3781

## Alec Elsbernd

### Assignment 5 Requirments:

Expanding upon the high-volume home office supply company’s data tracking requirements from assignment 4, the CFO
requests your services again to extend the data model’s functionality. The CFO has read about the
capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a
smaller data mart as a test platform. He is under pressure from the members of the company’s board of
directors who want to review more detailed sales reports based upon the following measurements:

1. Product
2. Customer
3. Sales representative
4. Time (year, quarter, month, week, day, time)
5. Location

Furthermore, the board members want location to be expanded to include the following characteristics of
location:

1. Region
2. State
3. City
4. Store



#### Assignment Screenshots:


![diagram](img/a5_erd.png)



![select satements](img/a5_1.png)



![select staements](img/a5_2.png)



![select statements](img/a5_3.png)


![select statements](img/a5_4.png)


![select statements](img/a5_5.png)
