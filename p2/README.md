# LIS3781

## Alec Elsbernd

### Project 2 Requirements:

- MongoDB Create & Insert Database
- Add MongoDB Array using insert()
- Mongodb ObjectId()
- MongoDB Query Document using find()
- MongoDB cursor
- MongoDB Query Modifications using limit(), sort()
- MongoDB Count() & remove() function
- MongoDB Update() Document


#### Project 2 Screenshots


![select statemnts](img/p2_select.png)


![select statements](img/p2_select2.png)


![select statements](img/p2_select3.png)


![select statements](img/p2_select4.png)

