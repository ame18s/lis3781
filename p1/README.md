# LIS3781

## Alec Elsbernd

### Project 1 Requirements:

As the lead DBA for a local municipality, you are contacted by the city council to design a database in order to track and document the city’scourt case data. Some report examples: Which attorney is assigned to 
what case(s)?How many unique clients have cases(be sureto add a client tomore than one case)?How many cases has each attorney been assigned, and names of their clients (return number and names)?How many cases does each client 
have with the firm(return a name and number value)?Which types ofcases does/dideach client have/hadand their start and end dates?Which attorney is associated to which client(s), and to which case(s)?Names of three judges with the 
most number of years in practice, includenumber of years.Also, include the following business rules:


• An attorney is retainedby (or assigned to)one or moreclients,for each case.• A client has(or is assigned to) one or more attorneysfor each case.

• An attorney hasone or more cases.• A client has one or more cases.

• Each court has one or more judges adjudicating.

• Each judge adjudicates upon exactly one court.

• Each judgemaypresideovermore than one case.

• Each case that goes to court ispresided over by exactly one judge.

• A person can have more than one phone number.


#### Project 1 Screenshots

![project 1 erd](img/p1erd.png)


![select statemnts](img/p1_select.png)


![select statements](img/p1_select2.png)


![select statements](img/p1_select3.png)


![select statements](img/p1_select4.png)

