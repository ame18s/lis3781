# LIS3781 Advanced Database Analysis

## Alec Elsbernd

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMMPS
    - Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutarial (bitbucketstationlocation)
	- Provide git command description

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Bitbucket repo
    - Complete Bitbucket tutorial 
	- Provide git command descriptions
	- screenshot of sql code and query results
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	
	- Install Oracle
	- Populate tables
	- screenshot of sql code and query results

3. [A4 README.md](a4/README.md "My A4 README.md file")
	
	- Install Oracle
	- Populate tables
	- screenshot of sql code and query results

4. [P1 README.md](p1/README.md "My P1 README.md file")
	
	- Install Oracle
	- Populate tables
	- screenshot of sql code and query results

3. [A5 README.md](a5/README.md "My A5 README.md file")
	
	- Install Oracle
	- Populate tables
	- screenshot of sql code and query results
    
6. [P2 README.md](p2/README.md "My P2 README.md file")

	- MongoDB Create & Insert Database
	- Add MongoDB Array using insert()
	- Mongodb ObjectId()
	- MongoDB Query Document using find()
	- MongoDB cursor
	- MongoDB Query Modifications using limit(), sort()
	- MongoDB Count() & remove() function
	- MongoDB Update() Document
